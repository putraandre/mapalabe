<?php
    class Alat{

        // Connection
        private $conn;

        // Table
        private $db_table = "alat";

        // Columns
        public $id_alat;
        public $nama;
        public $jumlah;
        public $kategori;
        public $deskripsi;

        // Db connection
        public function __construct($db){
            $this->conn = $db;
        }

        // GET ALL
        public function getAlat(){
            $sqlQuery = "SELECT id_alat, nama, jumlah, kategori, deskripsi FROM " . $this->db_table . "";
            $stmt = $this->conn->prepare($sqlQuery);
            $stmt->execute();
            return $stmt;
        }

        // CREATE
        public function createAlat(){
            $sqlQuery = "INSERT INTO
                        ". $this->db_table ."
                    SET
                        nama = :nama,
                        jumlah = :jumlah,
                        kategori = :kategori,
                        deskripsi = :deskripsi";
        
            $stmt = $this->conn->prepare($sqlQuery);
        
            // sanitize
            $this->nama=htmlspecialchars(strip_tags($this->nama));
            $this->jumlah=htmlspecialchars(strip_tags($this->jumlah));
            $this->kategori=htmlspecialchars(strip_tags($this->kategori));
            $this->deskripsi=htmlspecialchars(strip_tags($this->deskripsi));
        
            // bind data
            $stmt->bindParam(":nama", $this->nama);
            $stmt->bindParam(":jumlah", $this->jumlah);
            $stmt->bindParam(":kategori", $this->kategori);
            $stmt->bindParam(":deskripsi", $this->deskripsi);
        
            if($stmt->execute()){
               return true;
            }
            return false;
        }

        // READ single
        public function getSingleEmployee(){
            $sqlQuery = "SELECT
                        id, 
                        name, 
                        email, 
                        age, 
                        designation, 
                        created
                      FROM
                        ". $this->db_table ."
                    WHERE 
                       id = ?
                    LIMIT 0,1";

            $stmt = $this->conn->prepare($sqlQuery);

            $stmt->bindParam(1, $this->id);

            $stmt->execute();

            $dataRow = $stmt->fetch(PDO::FETCH_ASSOC);
            
            $this->name = $dataRow['name'];
            $this->email = $dataRow['email'];
            $this->age = $dataRow['age'];
            $this->designation = $dataRow['designation'];
            $this->created = $dataRow['created'];
        }        

        // UPDATE
        public function updateAlat(){
            $sqlQuery = "UPDATE
                        ". $this->db_table ."
                    SET
                      nama = :nama,
                      jumlah = :jumlah,
                      kategori = :kategori,
                      deskripsi = :deskripsi
                    WHERE 
                        id_alat = :id_alat";
        
            $stmt = $this->conn->prepare($sqlQuery);

            $this->nama=htmlspecialchars(strip_tags($this->nama));
            $this->jumlah=htmlspecialchars(strip_tags($this->jumlah));
            $this->kategori=htmlspecialchars(strip_tags($this->kategori));
            $this->deskripsi=htmlspecialchars(strip_tags($this->deskripsi));
            $this->id_alat=htmlspecialchars(strip_tags($this->id_alat));
        
            // bind data
            $stmt->bindParam(":nama", $this->nama);
            $stmt->bindParam(":jumlah", $this->jumlah);
            $stmt->bindParam(":kategori", $this->kategori);
            $stmt->bindParam(":deskripsi", $this->deskripsi);
            $stmt->bindParam(":id_alat", $this->id_alat);
        
            if($stmt->execute()){
               return true;
            }
            return false;
        }

        // DELETE
        function deleteAlat(){
            $sqlQuery = "DELETE FROM " . $this->db_table . " WHERE id_alat = ?";
            $stmt = $this->conn->prepare($sqlQuery);
        
            $this->id_alat=htmlspecialchars(strip_tags($this->id_alat));
        
            $stmt->bindParam(1, $this->id_alat);
        
            if($stmt->execute()){
                return true;
            }
            return false;
        }

    }
?>