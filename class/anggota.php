<?php
    class Anggota{

        // Connection
        private $conn;

        // Table
        private $db_table = "anggota";

        // Columns
        public $id_anggota;
        public $nama;
        public $alamat;
        public $email;
        public $nohp;

        // Db connection
        public function __construct($db){
            $this->conn = $db;
        }

        // GET ALL
        public function getAnggota(){
            $sqlQuery = "SELECT id_anggota, nama, alamat, email, nohp FROM " . $this->db_table . "";
            $stmt = $this->conn->prepare($sqlQuery);
            $stmt->execute();
            return $stmt;
        }

        // CREATE
        public function createAnggota(){
            $sqlQuery = "INSERT INTO
                        ". $this->db_table ."
                    SET
                        nama = :nama,
                        alamat = :alamat,
                        email = :email,
                        nohp = :nohp";
        
            $stmt = $this->conn->prepare($sqlQuery);
        
            // sanitize
            $this->nama=htmlspecialchars(strip_tags($this->nama));
            $this->alamat=htmlspecialchars(strip_tags($this->alamat));
            $this->email=htmlspecialchars(strip_tags($this->email));
            $this->nohp=htmlspecialchars(strip_tags($this->nohp));
        
            // bind data
            $stmt->bindParam(":nama", $this->nama);
            $stmt->bindParam(":alamat", $this->alamat);
            $stmt->bindParam(":email", $this->email);
            $stmt->bindParam(":nohp", $this->nohp);
        
            if($stmt->execute()){
               return true;
            }
            return false;
        }

        // READ single
        public function getSingleAnggota(){
            $sqlQuery = "SELECT
                        id_anggota, nama, alamat, email, nohp
                      FROM
                        ". $this->db_table ."
                    WHERE 
                    id_anggota = ?
                    LIMIT 0,1";

            $stmt = $this->conn->prepare($sqlQuery);

            $stmt->bindParam(1, $this->id_anggota);

            $stmt->execute();

            $dataRow = $stmt->fetch(PDO::FETCH_ASSOC);
            
            $this->nama=htmlspecialchars(strip_tags($this->nama));
            $this->alamat=htmlspecialchars(strip_tags($this->alamat));
            $this->email=htmlspecialchars(strip_tags($this->email));
            $this->nohp=htmlspecialchars(strip_tags($this->nohp));
        }        

        // UPDATE
        public function updateAnggota(){
            $sqlQuery = "UPDATE
                        ". $this->db_table ."
                    SET
                      nama = :nama,
                      alamat = :alamat,
                      email = :email,
                      nohp = :nohp
                    WHERE 
                        id_anggota = :id_anggota";
        
            $stmt = $this->conn->prepare($sqlQuery);
            $this->nama=htmlspecialchars(strip_tags($this->nama));
            $this->alamat=htmlspecialchars(strip_tags($this->alamat));
            $this->email=htmlspecialchars(strip_tags($this->email));
            $this->nohp=htmlspecialchars(strip_tags($this->nohp));
            $this->id_anggota=htmlspecialchars(strip_tags($this->id_anggota));
            
            // bind data
            $stmt->bindParam(":nama", $this->nama);
            $stmt->bindParam(":alamat", $this->alamat);
            $stmt->bindParam(":email", $this->email);
            $stmt->bindParam(":nohp", $this->nohp);
            $stmt->bindParam(":id_anggota", $this->id_anggota);
        
            if($stmt->execute()){
               return true;
            }
            return false;
        }

        // DELETE
        function deleteAnggota(){
            $sqlQuery = "DELETE FROM " . $this->db_table . " WHERE id_anggota = ?";
            $stmt = $this->conn->prepare($sqlQuery);
        
            $this->id_anggota=htmlspecialchars(strip_tags($this->id_anggota));
        
            $stmt->bindParam(1, $this->id_anggota);
        
            if($stmt->execute()){
                return true;
            }
            return false;
        }

    }
?>