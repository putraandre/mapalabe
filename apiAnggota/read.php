<?php
    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json; charset=UTF-8");
    header("Access-Control-Allow-Methods: POST");
    header("Access-Control-Max-Age: 3600");
    header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
    
    include_once '../config/database.php';
    include_once '../class/anggota.php';

    $database = new Database();
    $db = $database->getConnection();

    $items = new Anggota($db);

    $stmt = $items->getAnggota();
    $itemCount = $stmt->rowCount();

    if($itemCount > 0){
        
        $barangArr = array();
        $barangArr["body"] = array();
        $barangArr["itemCount"] = $itemCount;

        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
            extract($row);
            $e = array(
                "id" => $id_anggota,
                "nama" => $nama,
                "alamat" => $alamat,
                "email" => $email,
                "nohp" => $nohp
            );

            array_push($barangArr["body"], $e);
        }
        echo json_encode($barangArr);
    }

    else{
        http_response_code(404);
        echo json_encode(
            array("message" => "No record found.")
        );
    }
?>