<?php
  if (isset($_SERVER['HTTP_ORIGIN'])) {
          header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
          header('Access-Control-Allow-Credentials: true');
          header('Access-Control-Max-Age: 86400');    // cache for 1 day
      }
      
      // Access-Control headers are received during OPTIONS requests
      if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
      
          if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
              header("Access-Control-Allow-Methods: GET, POST, OPTIONS");         
      
          if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
              header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
      
          exit(0);
  }    
      include_once '../config/database.php';
      include_once '../class/anggota.php';
      
      $database = new Database();
      $db = $database->getConnection();
      
      $item = new Anggota($db);
      
      $data = json_decode(file_get_contents("php://input"));
      if (
          !empty($data->id_anggota)&&
          !empty($data->nama) &&
          !empty($data->alamat)&&
          !empty($data->email) &&
          !empty($data->nohp)
      ) {
      $item->id_anggota = $data->id_anggota;
      $item->nama = $data->nama;
      $item->alamat = $data->alamat;
      $item->email = $data->email;
      $item->nohp = $data->nohp;
      
      if($item->updateAnggota()){
          echo json_encode(array("message" => "data anggota updated."));
      } else{
          echo json_encode(array("message" => "Data anggota could not be updated"));
      }
  }else {
      http_response_code(400);
      echo json_encode(array("message" => "Unable to update product. Data is incomplete."));
  }
?>