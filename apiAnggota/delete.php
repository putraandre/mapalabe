<?php
  header("Access-Control-Allow-Origin: *");
  header("Content-Type: application/json; charset=UTF-8");
  header("Access-Control-Allow-Methods: DELETE");
  header("Access-Control-Max-Age: 3600");
  header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

  include_once '../config/database.php';
  include_once '../class/anggota.php';

  $request = $_SERVER['REQUEST_METHOD'];

  switch ($request) {
      case 'DELETE':
          $data = json_decode(file_get_contents("php://input"));
          deletemethod($data);
          break;

      default:
          echo '{"status":"405","result": "Method Not Allowed"}';
          break;
  }
  function deletemethod($data){
      $database = new Database();
      $db = $database->getConnection();

      $item = new Anggota($db);
      $item->id_anggota = isset($_GET['id_anggota']) ? $_GET['id_anggota'] : die();
      if (
          !empty($item->id_anggota)
      ) {
          // $item->id = $data->id;

          if ($item->deleteAnggota()) {
              echo json_encode(array("message" => "Data deleted."));
          } else {
              echo json_encode(array("message" => "Data could not be deleted"));
          }
      } else {
          http_response_code(400);
          echo json_encode(array("message" => "Unable to delete product. Data is incomplete."));
      }
  }